#!/bin/bash
## 
## After Install Script 
##
## Run this script in a terminal once you have finished your Ubuntu 14.04 or
## Linux Mint 17 installation.
##

echo ""
echo " After Install Script."
echo " Department of Astronomy - IAG/USP"
echo ""

echo -n "Enter your name and press [ENTER]: "
read name
echo -n "Enter your gender and press [ENTER]: "
read email
echo 

echo " Updating files first."
apt-get update

echo " Installing aptitude."
apt-get install --yes --force-yes aptitude

echo " Installing compilators"
apt-get install --yes --force-yes gfortran g++

echo " Installing Python and Python Libs, graphical editors, latex, etc."
# Python Libs
apt-get install --yes --force-yes python-dev python-pip python-tk python-numpy python-scipy python-matplotlib python-pyfits ipython ipython-notebook
apt-get install --yes --force-yes python-pygments python3-pygments

# Text editor on terminals
apt-get install --yes --force-yes vim vim-addon-manager vim-python-jedi 

# Music/Video Player
apt-get install --yes --force-yes vlc 

# Version controler
apt-get install --yes --force-yes git 

# Photo/Drawing
apt-get install --yes --force-yes inkscape gimp gphoto2 ufraw gimp-ufraw

# TeX
apt-get install --yes --force-yes texlive-latex-base-doc texlive-latex-recommended texlive-latex-recommended-doc texlive-latex-extra texlive-latex-extra-doc texlive-lang-portuguese texlive-science latex-beamer kile texlive-latex3  texlive-latex-extra texlive-latex3  texlive-latex-extra texlive-math-extra texlive-math-extra

# Desktop terminal 
apt-get install --yes --force-yes guake 

# PDF reader
apt-get install --yes --force-yes okular 

# SSH
apt-get install --yes --force-yes openssh-server

# VNC
apt-get install --yes --force-yes remmina remmina-plugin-vnc 

# Mount folders/disks over the network
apt-get install --yes --force-yes nfs-common nfs-client nfs-server  

# Before installing IRAF
apt-get install --yes --force-yes csh saods9

# Cisco VPN client
apt-get install --yes --force-yes vpnc  

# Before install PyRAF
apt-get install --yes --force-yes xorg-dev libx11-dev 

# Drop and Virtual Boxes
apt-get install --yes --force-yes dropbox virtualbox virtualbox-guest-utils 

# Zip and Unzip
apt-get install --yes --force-yes p7zip-full atool

# Documents/References handling
apt-get install --yes --force-yes mendeleydesktop 

# Cairo Dock - a fancy dock
apt-get install --yes --force-yes cairo-dock cairo-clock cairo-dock-plug-ins cairo-dock-data cairo-perf-utils cairo-dock-plug-ins-integration
apt-get install --yes --force-yes compiz compiz-plugins gnome-session alacarte cairo-dock-plug-ins-dbus-interface-mono  cairo-dock-plug-ins-dbus-interface-ruby cairo-dock-plug-ins-dbus-interface-vala gnote indicator-bluetooth indicator-messages indicator-network indicator-printers indicator-sound

# Skype
apt-get install --yes --force-yes skype

# Web Design
apt-get install --yes --force-yes npm node
npm install -g bower
bower install material-design-icons

# Math and plot tools
apt-get install --yes --force-yes cl-plplot
apt-get install --yes --force-yes liblapack-dev liblapack-doc liblapack-test liblapacke liblapacke-dev libopenblas-base libopenblas-dev

# Install LAMP (Linux, Apache, MariaDB [a replacement for MySQL], PHP5)
# Apache
apt-get install --yes --force-yes apache2 xdg-utils
echo "ServerName localhost" | sudo tee /etc/apache2/conf-available/servername.conf
a2enconf servername
/etc/init.d/apache2 restart
xdg-open http://localhost/ &

# PHP
apt-get install --yes --force-yes php5 libapache2-mod-php5
/etc/init.d/apache2 restart
echo "<?php phpinfo(); ?>" > /var/www/html/testphp.php
chmod a+r /var/www/html/testphp.php 
xdg-open http://localhost/testphp.php &

# MariaDB
apt-get install --yes --force-yes software-properties-common
apt-key adv --recv-keys --keyserver keyserver.ubuntu.com 0xcbcb082a1bb943db
add-apt-repository 'deb http://download.nus.edu.sg/mirror/mariadb/repo/10.0/ubuntu trusty main'
apt-get update && upgrade
apt-get install --yes --force-yes mariadb-server mariadb-client
mysql_secure_installation
service mysql restart


# More on Python
echo ""
echo " Installing/Upgrading Python Libraries"
echo ""
pip install numpy --upgrade
pip install scipy --upgrade
pip install matplotlib --upgrade
pip install astropy --upgrade
pip install watchdog --upgrade
pip install argparse --upgrade

# Java
echo ""
echo " Installing Oracle Java"
echo ""
apt-get purge --yes openjdk*
add-apt-repository --yes ppa:webupd8team/java
apt-get update
apt-get install --yes --force-yes oracle-java7-installer

# QFitsView
echo ""
echo " Downloading and installing QFitsView for FITS data visualization." 
echo ""
wget http://www.mpe.mpg.de/~ott/QFitsView/QFitsView_3.1.linux64 
chmod a+x QFitsView_3.1.linux64
mv QFitsView_3.1.linux64 /usr/bin/QFitsView
ln -s /usr/bin/QFitsView /usr/bin/qfitsview

echo ""
echo " Config GIT"
echo ""
git config --global user.name "$name"
git config --global user.email "$email"

echo ""
echo " Fix cedilla."
echo ""
sed -i "s/az:ca:co:fr:gv:oc:pt:sq:tr:wa/az:ca:co:fr:gv:oc:pt:sq:tr:wa:en/" /usr/lib/x86_64-linux-gnu/gtk-3.0/3.0.0/immodules.cache
sed -i "s/az:ca:co:fr:gv:oc:pt:sq:tr:wa/az:ca:co:fr:gv:oc:pt:sq:tr:wa:en/" /usr/lib/x86_64-linux-gnu/gtk-2.0/2.10.0/immodules.cache
sed -i "s/ć/ç/g" /usr/share/X11/locale/en_US.UTF-8/Compose
sed -i "s/Ć/Ç/g" /usr/share/X11/locale/en_US.UTF-8/Compose
echo "GTK_IM_MODULE=cedilla" >> /etc/environment
echo "QT_IM_MODULE=cedilla" >> /etc/environment
reboot now
